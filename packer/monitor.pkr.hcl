locals {
  base_image_name            = "local:base-ubuntu-20.04"
  image_name                 = "monitor-ubuntu-20.04"
  ansible_role_loki          = "andreswebs.loki"
  ansible_role_prometheus    = "andreswebs.prometheus"
  ansible_role_grafana       = "andreswebs.grafana"
}

source "lxd" "monitor" {

  image        = local.base_image_name
  output_image = local.image_name

  publish_properties = {
    description = "Monitor image from ${local.base_image_name}"
  }

}

locals {
  playbook_dir = "${path.root}/../ansible"
}

build {
  sources = ["source.lxd.monitor"]

  provisioner "shell" {
    inline = [
      "ansible-galaxy install ${local.ansible_role_loki}",
      "ansible-galaxy install ${local.ansible_role_prometheus}",
      "ansible-galaxy install ${local.ansible_role_grafana}",
    ]
  }

  provisioner "ansible-local" {
    clean_staging_directory = true
    command                 = "ANSIBLE_LOCALHOST_WARNING=false ansible-playbook"
    playbook_dir            = local.playbook_dir
    playbook_file           = "${local.playbook_dir}/monitor.playbook.yml"
  }

}
