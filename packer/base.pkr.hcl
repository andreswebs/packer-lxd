locals {
  image_name = "base-ubuntu-20.04"
}

source "lxd" "base" {

  image        = "images:ubuntu/focal/amd64"
  output_image = local.image_name

  publish_properties = {
    description = "Base image from ubuntu 20.04"
  }

}

locals {
  playbook_dir = "${path.root}/../ansible"
}

build {
  sources = ["source.lxd.base"]

  provisioner "shell" {
    inline = [
      "apt-get update --quiet && apt-get install --quiet --yes ansible"
    ]
  }

  provisioner "ansible-local" {
    clean_staging_directory = true
    command                 = "ANSIBLE_LOCALHOST_WARNING=false ansible-playbook"
    playbook_dir            = local.playbook_dir
    playbook_file           = "${local.playbook_dir}/base.playbook.yml"
  }

}
