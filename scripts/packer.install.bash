#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

function finish {
  rm -rf "${WORKDIR}"
}

BIN_NAME="packer"
OS="linux"
ARCH="amd64"

PACKER_VERSION="${PACKER_VERSION:-1.7.9}"
PACKER_INSTALL_DIR="${PACKER_INSTALL_DIR:-/usr/local/bin}"

FILE_NAME="${BIN_NAME}_${PACKER_VERSION}_${OS}_${ARCH}.zip"
DOWNLOAD_URL="https://releases.hashicorp.com/${BIN_NAME}/${PACKER_VERSION}/${FILE_NAME}"

WORKDIR="$(mktemp -d -t ${BIN_NAME}.XXXXXXXXX)"
TMP_FILE="${WORKDIR}/${FILE_NAME}"

## main

trap finish EXIT

echo "Downloading ${BIN_NAME} ${PACKER_VERSION} ..."
curl --silent --location --output "${TMP_FILE}" "${DOWNLOAD_URL}"


echo "Installing ${BIN_NAME} ${PACKER_VERSION} ..."
unzip -qq "${TMP_FILE}" -d "${WORKDIR}"
install "${WORKDIR}/${BIN_NAME}" "${PACKER_INSTALL_DIR}"

echo "done."
