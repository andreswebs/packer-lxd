#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SCRIPT_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
TEMPLATES_DIR="${SCRIPT_PATH}/../packer"

for t in "${TEMPLATES_DIR}"/*.pkr.hcl; do
  echo "Building template: ${t}"
  [ -f "${t}" ] && {
    packer build "${t}"
  }
done